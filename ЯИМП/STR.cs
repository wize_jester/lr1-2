﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ЯИМП
{
    class STR
    {
        StringBuilder Line;
        private int n;
        public int N
        {
        get{ return n; }
        }
        public char this[int i]
        {
            set { Line[i] = Convert.ToChar(value); }
            get { return Line[i]; }
        }
        public void Create()
        {
            Line = new StringBuilder();
            Line.Append(Console.ReadLine());
            n = Line.Length;
            
        }
        public int Space()
        {
            int c = 0;
            for (int i = 0; i < n; i++)
                if (Line[i] == ' ')
                    c++;

            return c;
        }
        public StringBuilder ToLow()
        {
            string s1 = Convert.ToString(Line);
            for (int i = 0; i < n; i++)
                s1=s1.ToLower();
            Line.Remove(0, n);
            Line.Append(s1);
                    return Line;
        }

        public StringBuilder DeletePuncs()
        {
            char[] split = new char[] { ',', '.', '!', '?', '-',':','(',')','\'','\"' };
            for (int i = 0; i <=n-1; i++)
                if (split.Contains(Line[i]))
                {
                    Line.Remove(i, 1);

                }
                    return Line;
        }
    }
}
