﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЯИМП
{
    class Field2
    {
        public int[,] IntArray;
        private static int n;
        public static int N
        {
            set { n = value; }
            get { return n; }
        }
        public int this[int i, int j]
        {
            set { IntArray[i, j] = value; }
            get { return IntArray[i, j]; }

        }

        public int Zero
        {
            get
            {
                int z = 0;
                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        if (IntArray[i, j] == 0)
                            z += 1;
                    }

                }
                return z;
            }
        }
        public int Diag
        {
            set
            {


                for (int i = 0; i < N; i++)
                    IntArray[i, i] = value;
            }
        }
        public int[,] Create(int N)
        {
            IntArray = new int[N, N];


            return IntArray;
        }
        private int[,] Fill(string s, int I)
        {
            Random R = new Random();
            if (s.Length == 0)
            {
                for (int j = 0; j < n; j++)
                    IntArray[I, j] = R.Next(0, 10);
                return IntArray;
            }
            char[] split = new char[] { ',', '.', ' ', '/', '-' };

            string[] ar = s.Split(split);
            
            for (int i = 0; i <= n - 1; i++)
            {
                IntArray[I, i] = Convert.ToInt32(ar[i]);
            }
            return IntArray;

        }
        public int[,] fill(int[,] a)
        {
            string s;
            for (int i = 0; i <= N - 1; i++)
            {
                s = Console.ReadLine();
                IntArray = Fill(s, i);
            }
            return IntArray;

        }
        public void Write()
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                    Console.Write(IntArray[i, j] + "  ");
                Console.WriteLine();
                Console.WriteLine();
            }
        }
        public void Bolt()
        {
            int s = 0;
            Console.Write("Введите столбец: ");
            int j = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < N; i++)
                s += IntArray[i, j - 1];
            Console.WriteLine(s);
        }
        public static Field2 operator ++(Field2 IntArray)
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                    IntArray[i, j] += 1;
            }
            return IntArray;
        }
        public static Field2 operator --(Field2 IntArray)
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                    IntArray[i, j] -= 1;

            }
            return IntArray;
        }
        
    }
}
